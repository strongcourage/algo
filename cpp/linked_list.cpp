#include <iostream>

using namespace std;

template <class datatype>
class LinkedList {
private:
	LinkedList *head;
public:
	datatype data;
	LinkedList *next;
	
	// constructors
	LinkedList() {
		head = NULL;
		next = NULL;
	}

	void insertAtBeginning(datatype data) {
		LinkedList<datatype> *newNode = new LinkedList<datatype>();
		newNode->data = data;
		newNode->next = head;
		head = newNode;
	}

	void deleteAtBeginning() {
		LinkedList *temp = head;
		head = head->next;
		delete temp;
	}

	// Note: position >= 0, similar to array index
	void insertAtPosition(datatype data, int position) {
		LinkedList<datatype> *newNode = new LinkedList<datatype>();
		newNode->data = data;

		LinkedList *temp = head;
		for (int i=0; i < position - 1; ++i) {
			temp = temp->next;
		} // temp will at (position-1)

		newNode->next = temp->next;
		temp->next = newNode;
	}

	// Note: position >= 0, similar to array index
	void deleteAtPosition(int position) {
		LinkedList *temp = head;
		for (int i=0; i < position - 1; ++i) {
			temp = temp->next;
		}

		LinkedList *nodeAtPosition = temp->next;
		temp->next = nodeAtPosition->next;
		delete nodeAtPosition;
	}

	void insertAtEnd(datatype data) {
		LinkedList *temp = head;
		while (temp->next != NULL) {
			temp = temp->next;
		}

		LinkedList<datatype> *newNode = new LinkedList<datatype>();
		newNode->data = data;
		temp->next = newNode;
		newNode->next = NULL;
	}

	void deleteAtEnd() {
		LinkedList *temp = head;
		while (temp->next->next != NULL) {
			temp = temp->next;
		}

		LinkedList *prevEndNode = temp;
		LinkedList *endNode = temp->next;
		prevEndNode->next = NULL;
		delete endNode;
	}

	bool isEmpty() {
		return (head == NULL);
	}

	void search(int element) {
		LinkedList *temp = head;
		LinkedList<datatype> *node = new LinkedList<datatype>();
		int count = 0;
		while (temp->next != NULL) {
			++count;
			if (temp->data == element) {
				cout << "Found element at " << count << " of linked list." << endl;
				return;
			}
			temp = temp->next;
		}
		cout << "Could not find " << element << " in the linked list." << endl;
	}

	// Understand ?
	void reverse() {
		LinkedList* current = head;
		LinkedList* prev = NULL;
		LinkedList* nextNode;

		while(current != NULL){
			nextNode = current->next;
			current->next = prev;
			prev = current;
			current = nextNode;
		}
		head = prev;
	}

	void display() {
		LinkedList *temp = head;
		while (temp != NULL) {
			if (temp->next != NULL) {
				cout << temp->data << " | ";
			} else {
				cout << temp->data << endl; // end node
			}
			temp = temp->next;
		}
	}
};



int main() {
	LinkedList<int>* list = new LinkedList<int>();
	list->insertAtBeginning(2);
	list->insertAtBeginning(4);
	list->insertAtBeginning(5);
	list->insertAtBeginning(3);
	list->insertAtBeginning(1);
	list->insertAtBeginning(6);	

	list->display();
	cout << "Insert 9 at position 3: " << endl; 
	list->insertAtPosition(9, 3);
	list->display();
	cout << "Delete 9 from position 3: " << endl; 
	list->deleteAtPosition(3);
	list->display();
	cout << "Insert 11 at end: " << endl; 
	list->insertAtEnd(11);
	list->display();
	cout << "Delete from end: " << endl; 
	list->deleteAtEnd();
	list->display();
	list->search(5);
	list->reverse();
	cout << "Reverse linked list: " << endl; 
	list->display();
	return 0;
}