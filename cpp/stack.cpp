#include <iostream>

using namespace std;

template <class datatype>
class StackInterface {
public:
	virtual void push(datatype data) = 0;
	virtual datatype peek() = 0;
	virtual void pop() = 0;
	virtual bool isEmpty() = 0;
	virtual bool isFull() {
		return true;
	}
	virtual void display() = 0;
};

template <class datatype>
class StackAsArray : public StackInterface<datatype> {
private:
	int top;
	int size;
	datatype* array;
public:
	StackAsArray() {
		this->size = 100;
		this->top = -1;
		array = new datatype[100];
	}

	StackAsArray(int size) {
		this->size = size;
		this->top = -1;
		array = new datatype[size];
	}

	void push(datatype data) {
		if (!isFull()) {
			array[++top] = data;
		} else {
			cout << "The stack in full of contents, cannot push " << data << " anymore" << endl;
		}
	}

	datatype peek() {
		if (!isFull()) {
			return array[top];
		} 

		cout << "There are no elements in the stack" << endl;
	}

	void pop() {
		if (!isEmpty()) {
			--top;
		} else {
			cout << "There are no elements in the stack to pop" << endl;
		}
	}

	bool isFull() {
		return (top == size - 1);
	}

	bool isEmpty() {
		return (top == -1);
	}

	void display() {
		if (!isEmpty()) {
			for (int i=0; i<size; ++i) {
				if (i != top) {
					cout << array[i] << " | ";
				} else {
					cout << array[i] << endl;
				}
			}
		} else {
			cout << "The stack is empty." << endl;
		}
	}
};

template <class datatype>
class StackAsLinkedList : public StackInterface<datatype> {
private:
	StackAsLinkedList* head;
public:
	datatype data;
	StackAsLinkedList* next;

	StackAsLinkedList() {
		head = NULL;
	}

	void push(datatype data) {
		StackAsLinkedList<datatype>* newNode = new StackAsLinkedList<datatype>();
		newNode->data = data;
		newNode->next = head;
		head = newNode;
	}

	void pop() {
		if (!isEmpty()) {
			StackAsLinkedList* headNode = head;
			head = head->next;
			delete headNode;
		} else {
			cout << "There are no elements in the stack to pop" << endl;
		}
	}

	bool isEmpty() {
		return (head == NULL);
	}

	datatype peek() {
		if (!isEmpty()) {
			return head->data;
		} 

		cout << "There are no elements in the stack" << endl;
		return -1;
	}

	void display() {
		if (!isEmpty()) {
			StackAsLinkedList* temp = head;
			while (temp != NULL) {
				if (temp->next != NULL) {
					cout << temp->data << " | ";
				} else {
					cout << temp->data << endl;
				}
				temp = temp->next;
			}
		} else {
			cout << "The stack is empty." << endl;
		}
	}
};


template <class datatype>
class StackFacade {
public:
	static StackInterface<datatype>* getInstance(string type = "linkedlist", int size = 50) {
		if(type == "array") {
			cout << "StackAsArray" << endl;
			return new StackAsArray<datatype>(size);
		} else {
			cout << "StackAsLinkedList" << endl;
			return new StackAsLinkedList<datatype>();
		}
	}
};

int main () {
	StackInterface<int>* stack = StackFacade<int>::getInstance(); // StackAsLinkedList
	// StackInterface<int>* stack = StackFacade<int>::getInstance("array", 50); // StackAsArray
	int n, data;
	do {
		cout << "--------- Menu ---------" << endl;
		cout << "1. Push" << endl;
		cout << "2. Pop" << endl;
		cout << "3. Peek" << endl;
		cout << "4. Display contents" << endl;
		cout << "5. Exit" << endl;
		cout << "Enter your choice(1-4)" << endl;
		cin >> n;
		switch(n){
			case 1:	cout << "Enter an value: " << endl;
					cin >> data;
					stack->push(data);
			break;
			case 2:	stack->pop(); 
					break;
			case 3:	cout << "Peek: " << stack->peek() << endl; 
					break;
			case 4:	cout << "Contents: "; 
					stack->display(); 
					break;
		}
	} while(n != 5);

	return 0;
}