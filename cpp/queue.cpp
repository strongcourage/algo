#include <iostream>

using namespace std;

template <class datatype>
class QueueInterface {
public:
	virtual void enqueue(datatype data) = 0;
	virtual void dequeue() = 0;
	virtual bool isEmpty() = 0;
	virtual bool isFull() {
		return true;
	}
	virtual void display() = 0;
	virtual datatype getFront() = 0;
};

template <class datatype> 
class QueueAsArray : public QueueInterface<datatype> {
private:
	int front; // first element
	int rear; // last element
	int size;
	datatype* array;
public:
	QueueAsArray(int size) {
		this->front = -1;
		this->rear = -1;
		this->size = size;
		array = new datatype[size];
	}

	void enqueue(datatype data) {
		if (isEmpty()) {
			front = 0;
		}

		if (!isFull()) {
			array[++rear] = data;
		} else {
			cout << "The array is full, cannot add " << data << endl;
		}
	}

	datatype getFront() {
		if (!isEmpty()) {
			return array[front];
		} else {
			cout << "The queue is empty..." << endl;
		}
		return -1;
	}

	void dequeue() {
		++front;
		if (front > rear) {
			rear = front = -1;
		}
	}

	bool isEmpty() {
		return (front == -1 && rear == -1);
	}

	bool isFull() {
		return (rear == size - 1);
	}

	void display() {
		if (!isEmpty()) {
			for (int i=0; i<size; ++i) {
				if (i != rear) {
					cout << array[i] << " | ";
				} else {
					cout << array[i] << endl;
				}
			}
		} else {
			cout << "The queue is empty." << endl;
		}
	}
};


template <class datatype>
class QueueAsLinkedList : public QueueInterface<datatype> {
private:
	datatype data;
	QueueAsLinkedList* head;
	QueueAsLinkedList* tail;
	QueueAsLinkedList* next;
public:
	QueueAsLinkedList() {
		head = NULL;
	}

	void enqueue(datatype data) {
		QueueAsLinkedList<datatype>* newNode = new QueueAsLinkedList<datatype>();
		newNode->data = data;
		newNode->next = NULL;
		if (isEmpty()) {
			head = newNode;
			tail = newNode;
		} else {
			tail->next = newNode;
			tail = newNode;
		}
	}

	datatype getFront() {
		if (!isEmpty()) {
			return head->data;
		} else {
			cout << "The queue is empty." << endl;
		}
		return -1;
	}

	void dequeue() {
		if (!isEmpty()) {
			QueueAsLinkedList<datatype>* firstNode = head;
			head = head->next;
			delete firstNode;
		} else {
			cout << "The queue is empty." << endl;
		}
	}

	bool isEmpty() {
		return (head == NULL);
	}

	void display() {
		if (!isEmpty()) {
			QueueAsLinkedList<datatype>* temp = head;
			while (temp != NULL) {
				if (temp->next != NULL) {
					cout << temp->data << " | ";
				} else {
					cout << temp->data << endl;
				}
				temp = temp->next;
			}
		} else {
			cout << "The queue is empty." << endl;
		}
	}
};

template <class datatype> 
class QueueFacade {
public:
	static QueueInterface<datatype>* getInstance(string type = "linkedlist", int size = 4) {
		if (type == "array") {
			cout << "QueueAsArray" << endl;
			return new QueueAsArray<datatype>(size);
		} else {
			cout << "QueueAsLinkedList" << endl;
			return new QueueAsLinkedList<datatype>();
		}
	}
};

int main() {
	QueueInterface<int>* queue = QueueFacade<int>::getInstance();
	cout << "Enqueing 3..." << endl;
	queue->enqueue(3);
	cout << "Enqueing 1..." << endl;
	queue->enqueue(1);
	cout << "Enqueing 2..." << endl;
	queue->enqueue(2);
	cout << "Contents: " << endl;
	queue->display();
	cout << "Enqueing 5..." << endl;
	queue->enqueue(5);
	cout << "Contents: " << endl;
	queue->display();
	cout << "Enqueing 9..." << endl;
	queue->enqueue(9);
	cout << "Contents: " << endl;
	queue->display();
	cout << "Dequeing..." << endl;
	queue->dequeue();
	cout << "Contents: " << endl;
	queue->display();
	cout << "Dequeing..." << endl;
	queue->dequeue();
	cout << "Dequeing..." << endl;
	queue->dequeue();
	cout << "Contents: " << endl;
	queue->display();
	cout << "Dequeing..." << endl;
	queue->dequeue();
	cout << "Contents: " << endl;
	queue->display();
	cout << "Dequeing..." << endl;
	queue->dequeue();
	cout << "Contents: " << endl;
	queue->display();
	return 0;
}