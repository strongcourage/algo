#ifndef IO_CC_
#define IO_CC_

#include <iostream>

// Program 17.4 Graph-processing input/output interface
template <class Graph>
class IO {
public:
	static void show(const Graph &); // print a graph
	static void scanEZ(Graph &); //insert edges defined by pairs of integers on standard input
	static void scan(Graph &); // insert edges defined by pairs of symbols on standard input
};

// Program 17.3 A client function that prints a graph
template <class Graph>
void IO<Graph>::show(const Graph &G) {
	for (int s=0; s<G.V(); s++) {
		cout.width(2);
		cout << s << ":";
		typename Graph::adjIterator A(G, s);
		for (int t=A.beg(); !A.end(); t = A.nxt()) {
			cout.width(2);
			cout << t << ":";
		}
		cout << endl;
	}
}

// Ex 17.12
template <class Graph>
void IO<Graph>::scanEZ(Graph &G) {
	int v, w;
	char c;

	do {
		
		cout << "Enter two integers which represent two vertices of the graph. Press 'q' to exit." << endl;
		cin >> v >> w;
		if (v > G.V() || w > G.V() || v < 0 || w < 0) {
			cout << "Error value" << endl;
			continue;
		}
		G.insert(Edge(v, w));
		cin >> c;
	} while (c!='q');
	
}

// Ex 17.14
template <class Graph>
void IO<Graph>::scan(Graph &G) {

}

#endif