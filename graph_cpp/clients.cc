#ifndef CLIENTS_CC_
#define CLIENTS_CC_

#include <vector>

// Program 17.2 Example of a graph-processing client function
template <class Graph>
vector <Edge> edges(Graph &G) {
	int E = 0;
	vector <Edge> a(G.E());
	for (int v=0; v<G.V(); v++) {
		typename Graph::adjIterator A(G, v); // using keyword typename to tell compiler Graph::adjIterator is a type name, not a static member of Graph
		for (int w=A.beg(); !A.end(); w = A.nxt()) {
			if (G.directed() || v < w) {
				a[E++] = Edge(v, w);
			}
		}
	}
	return a;
}

#endif