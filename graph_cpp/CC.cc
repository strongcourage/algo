#ifndef CC_CC_
#define CC_CC_

// Program 17.5 Connectivity interface
template <class Graph>
class CC {
private:
	// implemetation-dependent code
public:
	CC(const Graph &);
	int count();
	bool connect(int, int);
};

#endif