#ifndef GRAPH_CC_
#define GRAPH_CC_

// Program 17.1 Graph ADT interface
struct Edge {
	int v, w; // vertex v and vertex w
	// a constructor function that creates an Edge from two given vertices
	Edge(int v = -1, int w = -1) : v(v), w(w) {} // initializer list
};

class GRAPH {
private:
	// Implementation-dependent code
	int vertices;
public:
	/*
		int: the number of vertices
		bool: the graph is undirected or directed, with undirected the default
	*/
	// GRAPH(int, bool);
	GRAPH(int v) {
		this->vertices = v;
	}
	// ~GRAPH();
	int V() const;
	int E() const;
	bool directed() const;
	int insert(Edge);
	int remove(Edge);
	bool edge(int, int);

	// Process each of the vertices adjacent to any given vertex
	class adjIterator {
	public:
		/*
			GRAPH: the given graph
			int: the given vertex
		*/
		adjIterator(const GRAPH &, int);
		int beg();
		int nxt();
		bool end();
	};
};

#endif