#include <iostream>
#include <stdlib.h>
#include <vector>
#include "GRAPH.cc"
#include "IO.cc"
#include "CC.cc"

using namespace std;

// Program 17.6 Example of a graph-processing client program
int main() {
	int V;
	cin >> V;
	GRAPH G(V);
	IO<GRAPH>::scan(G);
	if (V < 20) {
		IO<GRAPH>::show(G);
	}
	cout << G.E() << " edges ";
	CC<GRAPH> Gcc(G);
	cout << Gcc.count() << " components" << endl;
	return 0;
}
